/*GENERAR LA SIGUIENTE INFORMACION DE MANERA ALEATORIA PARA 20 PERSONAS

EDAD: VALORES ENTEROS ENTRE 19 Y 99 
ALTURA: VALORES REALES ENTRE 1.5 Y 2.5
PESO: VALORES REALES ENTRE 20 Y 130

-------SE CALCULARA EL IMC Y SE DETERMINARA LO SIGUIENTE-----------

MENOS DE 18.5 = BAJO EN PESO
18.5 - 24.9 = PESO SALUDABLE
25.0 - 29.9 = SOBREPESO
30.0 O MAS = OBESIDAD
*/ 
var edad = 0;
var altura = 0;
var peso = 0;
var altura2=0;
var IMC = 0;
let nivel = "";


var num = 0; //contador en el resultados
var sumaIMC = 0;
var promedioIMC = 0;

edad = document.getElementById("edad");
altura = document.getElementById("altura");
peso = document.getElementById("peso");

var validarGenerar = "false";
var validarCalcular = "false";

function Generar(){

    validarGenerar = "true";
    edad = Math.floor(Math.random() * (99 - 18) + 18);
    document.getElementById("edad").value = edad;

    altura = Math.random() * (2.5 - 1.5) + 1.5;
    altura = altura.toFixed(2);
    document.getElementById("altura").value = altura;

    peso = Math.random() * (130 - 20) + 20;
    peso = peso.toFixed(2);
    document.getElementById("peso").value = peso;
}



function Calcular(){
    if(validarGenerar == "true"){
        validarCalcular = "true";
        altura2 = altura * altura;

        IMC = peso / altura2;
        IMC = IMC.toFixed(1);

        document.getElementById("lblIMC").innerHTML = "IMC = " + IMC;
        
        if(IMC < 18.5){
            document.getElementById("nivel").innerHTML = "Nivel = BAJO EN PESO" 
            nivel = "BAJO EN PESO" 
        } else if(IMC < 24.9){ 
            document.getElementById("nivel").innerHTML = "Nivel = PESO SALUDABLE"
            nivel = "PESO SALUDABLE"
        } else if(IMC < 29.9){ 
            document.getElementById("nivel").innerHTML = "Nivel = SOBREPESO"
            nivel = "SOBREPESO"
        } else if(IMC >= 30){ 
            document.getElementById("nivel").innerHTML = "Nivel = OBESIDAD"
            nivel = "OBESIDAD"
        }
    }else{alert("Primero se ocupan generar los datos")}
}



function Registrar(){
    if(validarCalcular == "true"){
        if(document.getElementById("edad").value == "" || document.getElementById("altura").value == "" || document.getElementById("peso").value == "" ){
            alert("Llene todos los campos");
        }else{

        num++;

        document.getElementById("resulNum").innerHTML = document.getElementById("resulNum").innerHTML  + "<p>" + num + "</p>";
        document.getElementById("resulEdad").innerHTML = document.getElementById("resulEdad").innerHTML + "<p>" + edad + "</p>";
        document.getElementById("resulAltura").innerHTML = document.getElementById("resulAltura").innerHTML + "<p>" + altura + "</p>";
        document.getElementById("resulPeso").innerHTML = document.getElementById("resulPeso").innerHTML + "<p>" + peso + "</p>";
        document.getElementById("resulImc").innerHTML = document.getElementById("resulImc").innerHTML + "<p>" + IMC + "</p>";
        document.getElementById("resulNivel").innerHTML = document.getElementById("resulNivel").innerHTML + "<p>" + nivel + "</p>";
        
        if(num <= 20){
        sumaIMC = sumaIMC + parseFloat(IMC);
        promedioIMC = sumaIMC / num;
        console.log("SumaIMC: " + sumaIMC);
        console.log("promedioIMC " + promedioIMC);
        document.getElementById("imcPromedio").innerHTML = "" + promedioIMC;
        console.log("");
    }
            
    }

}else{alert("Primero se ocupa calcular el IMC")}
}


function Limpiar(){
    location.reload();
}
    